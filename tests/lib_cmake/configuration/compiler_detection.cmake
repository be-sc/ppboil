macro(practest_mock_gcc)
    unset(PRAC_CXX_COMPILER_DETECTED)
    unset(PRAC_C_COMPILER_DETECTED)
    set(CMAKE_CXX_COMPILER_ID GNU)
    set(CMAKE_C_COMPILER_ID GNU)
    set(CMAKE_CXX_SIMULATE_ID)
    set(CMAKE_C_SIMULATE_ID)
    prac_detect_cxx_and_c_compilers()
endmacro()


macro(practest_mock_msvc2017)
    unset(PRAC_CXX_COMPILER_DETECTED)
    unset(PRAC_C_COMPILER_DETECTED)
    set(CMAKE_CXX_COMPILER_ID MSVC)
    set(CMAKE_C_COMPILER_ID MSVC)
    set(CMAKE_CXX_SIMULATE_ID)
    set(CMAKE_C_SIMULATE_ID)
    set(MSVC_VERSION 1910)
    prac_detect_cxx_and_c_compilers()
endmacro()


macro(practest_compiler_detection
    compiler_id simulated_id
    is_gcc_compat is_gcc is_clang is_msvc is_msvc_compat
    defines_tmpl
    line
)
    # simulate the requested compiler
    unset(PRAC_CXX_COMPILER_DETECTED)
    unset(PRAC_C_COMPILER_DETECTED)
    set(CMAKE_CXX_COMPILER_ID ${compiler_id})
    set(CMAKE_C_COMPILER_ID ${compiler_id})
    set(CMAKE_CXX_SIMULATE_ID ${simulated_id})
    set(CMAKE_C_SIMULATE_ID ${simulated_id})

    start_test("compiler detected: ${compiler_id} ${simulated_id}")
        prac_detect_cxx_and_c_compilers()

        foreach (lang CXX C)
            # Prac must have detected the compilers
            check_true(PRAC_${lang}_COMPILER_DETECTED ${line})

            # Check the boolean detection variables.
            set(expected
                "${lang}: "
                "is_gcc_compat: ${is_gcc_compat} "
                "is_gcc: ${is_gcc} "
                "is_clang: ${is_clang} "
                "is_msvc: ${is_msvc} "
                "is_msvc_compat: ${is_msvc_compat}"
            )
            set(actual
                "${lang}: "
                "is_gcc_compat: ${PRAC_${lang}_GCC_COMPAT} "
                "is_gcc: ${PRAC_${lang}_GCC} "
                "is_clang: ${PRAC_${lang}_CLANG} "
                "is_msvc: ${PRAC_${lang}_MSVC} "
                "is_msvc_compat: ${PRAC_${lang}_MSVC_COMPAT}"
            )
            check_equal(expected "${actual}" ${line})

            # Check the list of PP defines.
            # Expected list is passed sorted and with "lang" in place of the language ID.
            list(SORT PRAC_${lang}_COMPILER_PP_DEFINES)
            string(REPLACE "lang" ${lang} defines "${defines_tmpl}")
            check_equal(PRAC_${lang}_COMPILER_PP_DEFINES "${defines}" ${line})
        endforeach()
    end_test()
endmacro()
