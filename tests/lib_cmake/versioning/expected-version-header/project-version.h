#ifndef TESTSUITE_VERSION_INFO
#define TESTSUITE_VERSION_INFO

#define TESTSUITE_VERSION_MAJOR 1
#define TESTSUITE_VERSION_MINOR 2
#define TESTSUITE_VERSION_PATCH 3
#define TESTSUITE_VERSION_PRE ""
#define TESTSUITE_VERSION_META ""

// major.minor.patch
#define TESTSUITE_VERSION "1.2.3"

// major.minor.patch-pre (-pre is optional)
#define TESTSUITE_VERSION_FULL "1.2.3"

// major.minor.patch-pre+meta (-pre and +meta are optional)
#define TESTSUITE_VERSION_FULL_META "1.2.3"

#endif  // TESTSUITE_VERSION_INFO
