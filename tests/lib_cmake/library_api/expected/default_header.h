#ifndef MYLIB_API_DEFINES
#define MYLIB_API_DEFINES

#if defined(MYLIB_STATIC_LIB)
#   define MYLIB_API
#   define MYLIB_INTERNAL
#else
#   if defined(MYLIB_BUILDING_LIB)
#       define MYLIB_API __attribute__((visibility("default")))
#   else
#       define MYLIB_API __attribute__((visibility("default")))
#   endif

#   define MYLIB_INTERNAL __attribute__((visibility("hidden")))
#endif

#define MYLIB_DEPRECATED __attribute__((deprecated))
#define MYLIB_DEPRECATED_MSG __attribute__((deprecated(msg)))

#define MYLIB_DEPRECATED_API MYLIB_API MYLIB_DEPRECATED
#define MYLIB_DEPRECATED_API_MSG(msg) MYLIB_API MYLIB_DEPRECATED_MSG(msg)

#define MYLIB_DEPRECATED_INTERNAL MYLIB_INTERNAL MYLIB_DEPRECATED
#define MYLIB_DEPRECATED_INTERNAL_MSG(msg) MYLIB_INTERNAL MYLIB_DEPRECATED_MSG(msg)

#endif  // MYLIB_API_DEFINES
