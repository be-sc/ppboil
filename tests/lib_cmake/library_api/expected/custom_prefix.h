#ifndef MOO_API_DEFINES
#define MOO_API_DEFINES

#if defined(MOO_STATIC_LIB)
#   define MOO_API
#   define MOO_INTERNAL
#else
#   if defined(MOO_BUILDING_LIB)
#       define MOO_API __attribute__((visibility("default")))
#   else
#       define MOO_API __attribute__((visibility("default")))
#   endif

#   define MOO_INTERNAL __attribute__((visibility("hidden")))
#endif

#define MOO_DEPRECATED __attribute__((deprecated))
#define MOO_DEPRECATED_MSG __attribute__((deprecated(msg)))

#define MOO_DEPRECATED_API MOO_API MOO_DEPRECATED
#define MOO_DEPRECATED_API_MSG(msg) MOO_API MOO_DEPRECATED_MSG(msg)

#define MOO_DEPRECATED_INTERNAL MOO_INTERNAL MOO_DEPRECATED
#define MOO_DEPRECATED_INTERNAL_MSG(msg) MOO_INTERNAL MOO_DEPRECATED_MSG(msg)

#endif  // MOO_API_DEFINES
