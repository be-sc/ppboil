#ifndef MYLIB_API_DEFINES
#define MYLIB_API_DEFINES

#if defined(MYLIB_STATIC_LIB)
#   define MYLIB_API
#   define MYLIB_INTERNAL
#else
#   if defined(MYLIB_BUILDING_LIB)
#       define MYLIB_API __attribute__((visibility("default")))
#   else
#       define MYLIB_API __attribute__((visibility("default")))
#   endif

#   define MYLIB_INTERNAL __attribute__((visibility("hidden")))
#endif

#endif  // MYLIB_API_DEFINES
