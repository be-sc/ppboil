#include <praxample/hello.hpp>
#include <praxample/version.hpp>
#include <iostream>

int main()
{
    praxample::say_hello();
    std::cout << "This is version " << PRAXAMPLE_VERSION_FULL_META << ".\n";
}
