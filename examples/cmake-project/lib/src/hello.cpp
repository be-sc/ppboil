#include <praxample/hello.hpp>

#include <iostream>

namespace praxample
{

void say_hello()
{
    std::cout << "The lib says: \"Hello, world!\"\n";
}

}  // namespace praxample
