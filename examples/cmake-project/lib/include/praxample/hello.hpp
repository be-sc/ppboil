#ifndef PRAC_EXAMPLE_HELLO_LIB
#define PRAC_EXAMPLE_HELLO_LIB

#include <praxample/api.hpp>

namespace praxample
{
void PRAXAMPLE_API say_hello();
}

#endif
