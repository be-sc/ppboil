// Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
/**
Provides RAII based scope guards for executing code when leaving a scope. Modelled after Andrei Alexandrescu’s CppCon 2015 talk “Declarative Control Flow”.

Provides three macros:

``PRAC_SCOPE_EXIT``
    Executes code unconditionally on scope exit.
``PRAC_SCOPE_SUCCESS``
    Executes code only when no exception is thrown in the scope.
``PRAC_SCOPE_FAIL``
    Executes code only when an exception is thrown in the scope.

The macros must be followed by a block containing the code to execute. For example::

    PRAC_SCOPE_EXIT {
        do_some_cleanup();
    };

This block is the body of a lambda. Note the mandatory semicolon at its end. The lambda catches everything by reference and takes no arguments. Exceptions may only be thrown from the lamdba body for ``PRAC_SCOPE_SUCCESS``.

The macro respect the ``PRAC_UNPREFIXED_MACROS`` `Import-time Configuration`_.
*/
#pragma once

#if !defined(__cpp_lib_uncaught_exceptions)
#error "The scope guards require support for std::uncaught_exceptions() from C++17."
#endif

#include <exception>
#include <type_traits>
#include <utility>

namespace prac
{
namespace detail
{

template<typename ExitFunc>
class BasicScopeGuard
{
public:
    explicit BasicScopeGuard(ExitFunc&& func) noexcept : m_func{std::move(func)} {}

    BasicScopeGuard(const BasicScopeGuard&) = delete;
    BasicScopeGuard& operator=(const BasicScopeGuard&) = delete;
    BasicScopeGuard(BasicScopeGuard&&) = delete;
    BasicScopeGuard& operator=(BasicScopeGuard&&) = delete;

protected:
    ExitFunc m_func;
};


template<typename ExitFunc>
class ScopeExit : public BasicScopeGuard<ExitFunc>
{
public:
    using BasicScopeGuard<ExitFunc>::BasicScopeGuard;
    ~ScopeExit() noexcept(false) { this->m_func(); }
};


template<typename ExitFunc, bool execute_on_exception>
class ScopeSuccessOrFail : public BasicScopeGuard<ExitFunc>
{
public:
    using BasicScopeGuard<ExitFunc>::BasicScopeGuard;

    ~ScopeSuccessOrFail() noexcept(execute_on_exception)
    {
        const bool new_exception_thrown = std::uncaught_exceptions() > m_exception_count;
        if (new_exception_thrown == execute_on_exception) {
            this->m_func();
        }
    }

private:
    int m_exception_count = std::uncaught_exceptions();
};


enum class ScopeExitTag
{
};
enum class ScopeSuccessTag
{
};
enum class ScopeFailTag
{
};
template<typename ExitFunc>
auto operator+(ScopeExitTag, ExitFunc&& func)
{
    return ScopeExit<typename std::decay_t<ExitFunc>>(std::forward<ExitFunc>(func));
}
template<typename ExitFunc>
auto operator+(ScopeSuccessTag, ExitFunc&& func)
{
    return ScopeSuccessOrFail<typename std::decay_t<ExitFunc>, false>(std::forward<ExitFunc>(func));
}
template<typename ExitFunc>
auto operator+(ScopeFailTag, ExitFunc&& func)
{
    return ScopeSuccessOrFail<typename std::decay_t<ExitFunc>, true>(std::forward<ExitFunc>(func));
}

}  // namespace detail
}  // namespace prac

#define PRAC_DETAIL_CONCAT_IMPL(x, y) x##y
#define PRAC_DETAIL_CONCAT(x, y) PRAC_DETAIL_CONCAT_IMPL(x, y)
#define PRAC_DETAIL_UNIQUE_VAR(name) PRAC_DETAIL_CONCAT(name, __LINE__)


#define PRAC_SCOPE_EXIT \
    auto PRAC_DETAIL_UNIQUE_VAR(on_scope_exit_) = ::prac::detail::ScopeExitTag() + [&]() noexcept

#define PRAC_SCOPE_SUCCESS \
    auto PRAC_DETAIL_UNIQUE_VAR(on_scope_success_) = ::prac::detail::ScopeSuccessTag() + [&]()

#define PRAC_SCOPE_FAIL \
    auto PRAC_DETAIL_UNIQUE_VAR(on_scope_fail_) = ::prac::detail::ScopeFailTag() + [&]() noexcept


#if defined(PRAC_UNPREFIXED_MACROS)
#define SCOPE_EXIT PRAC_SCOPE_EXIT
#define SCOPE_SUCCESS PRAC_SCOPE_SUCCESS
#define SCOPE_FAIL PRAC_SCOPE_FAIL
#endif
