// Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
#include <iostream>
#include <cstdlib>

namespace prac
{
namespace detail
{

void abort_after_failed_assert(
        const char* expr, const char* file, int line, const char* funcsig, const char* msg)
{
    std::cout.flush();
    std::cerr << file << ':' << line << ": error: assertion failed\n"
              << "    in function: " << funcsig << '\n'
              << "    expression: " << expr << '\n';

    if (msg) {
        std::cerr << "    " << msg << '\n';
    }

    std::cerr.flush();
    std::abort();
}

void abort_after_failed_assert(const char* expr, const char* file, int line, const char* funcsig)
{
    abort_after_failed_assert(expr, file, line, funcsig, nullptr);
}

}  // namespace internal
}  // namespace prac
