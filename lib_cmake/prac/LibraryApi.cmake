# Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
#[===[.rst:
Provides functionality for defining the API of a library as source code annotations. Also configures the symbol visibility for shared libraries. Generates a header file with the appropriate preprocessor defines. Use *LibraryApi* as an alternative for the built-in *GenerateExportHeader*.

Some background on why ``prac_configure_library_api()`` is a complete reimplementation of ``generate_export_header()`` instead of a convenience wrapper:

*GenerateExportHeader* uses a default naming scheme that focuses on the technical aspects of the problem, namely exporting (or not) symbols from a shared library. For example, for a library *foo* the macro used for marking a symbol for export is ``FOO_EXPORT``.

The problem with this naming scheme is its level of abstraction. From a library author’s point of view the important thing is defining the public API of the library. The export mechanism only provides the means to implement that higher-level goal. Consequently the macro from the example above should better be called something along the lines of ``FOO_API`` or ``FOO_PUBLIC_API``. It’s a clearer statement of the author’s actual intent.

Additionally the wrong level of abstraction leads to some situations where the macro names are weird or misleading. This happens on Windows when using a library as a dependency, for instance *foo* from above. In that situation the symbols are *imported* into the software being built, and the macro should really be called ``FOO_IMPORT``. Something similar happens when building a static library. Since exporting symbols is not a thing for static libraries the macro names don’t make sense at all. Raising the level of abstraction and naming the macros accordingly gets rid of these problems.

Sadly, some of the macro names in the header file created by *GenerateExportHeader* are hard-coded to the *export* naming scheme, which made the reimplementation necessary.
]===]
include_guard(GLOBAL)

set(pracdetailLibraryApi_here_dir "${CMAKE_CURRENT_LIST_DIR}")
include("${pracdetailLibraryApi_here_dir}/_private_parts.cmake")

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Configures the functionality for defining the API of a library. This includes:

* Setting the default symbol visibility to *hidden* and hiding inlines (for shared libraries).
* Setting up related private and public compile definitions for the target.
* Generating a header with the usual API definition/symbol visibility macros.

This function may only be called for library targets. ::

    prac_configure_library_api(
        <lib_target>
        [HEADER_FILE <filepath>]
        [PREFIX <name>]
        [PRAGMA_ONCE | INCLUDE_GUARD_NAME <name>]
        [STATIC_LIB_DEFINE <name>]
        [BUILDING_LIB_DEFINE <name>]
        [API_DEFINE <name>]
        [INTERNAL_DEFINE <name>]
        [OMIT_DEPRECATION_DEFINES | [
          [DEPRECATED_DEFINE <name>]
          [DEPRECATED_MSG_DEFINE <name>]
          [DEPRECATED_API_DEFINE <name>]
          [DEPRECATED_API_MSG_DEFINE <name>]
          [DEPRECATED_INTERNAL_DEFINE <name>]
          [DEPRECATED_INTERNAL_MSG_DEFINE <name>]
        ]
        [PREAMBLE <content>]
        [APPENDIX <content>]
    )

All the default macro names are prefixed with an upper case version of ``PREFIX``.

Parameters:

``<lib_target>``
    Name of an existing library target.
``HEADER_FILE <filepath>``
    Path to the generated header file. A relative path is interpreted as relative to ``CMAKE_CURRENT_BINARY_DIR``. Defaults to ``${CMAKE_CURRENT_BINARY_DIR}/<prefix>_api.h``
``PREFIX <name>``
    Prefix used for macro names and for the header file name. Defaults to ``<lib_target>``.
``PRAGMA_ONCE`` and ``INCLUDE_GUARD_NAME <name>``
    Configures the include guard style used by the header file. Defaults to an include guard macro with a name derived from ``PREFIX``.
``STATIC_LIB_DEFINE <name>``
    The macro name for the flag indicating a static library. Defaults to ``<PREFIX>_STATIC_LIB``. If the target is a static library this define is added to its public compile definitions.
``BUILDING_LIB_DEFINE <name>``
    The macro name indicating that the library is built, not used. Defaults to ``<PREFIX>_BUILDING_LIB``. The target’s ``DEFINE_SYMBOL`` property is set to this value. Also used in the header file to detect how the header is used.
``API_DEFINE <name>``
    The macro name for marking symbols as part of the library’s public API. Such symbols are exported from shared libraries.
``INTERNAL_DEFINE <name>``
    The macro name for explicitly marking symbols as an internal implementation detail. Such symbols are not exported from shared libraries if the compiler supports that feature. Generally GCC and Clang do, MSVC does not.
``OMIT_DEPRECATION_DEFINES``
    If specified the deprecation macros are omitted from the generated header. All the other ``DEPRECATED_`` arguments are ignored.
``DEPRECATED_DEFINE <name>``, ``DEPRECATED_MSG_DEFINE <name>``, etc.
    The macro names for marking symbols as deprecated. Default names are based on the argument names. With the ``MSG`` variants it’s possible to provide an explaning message for each deprecated symbol. Also, convenience macros are available for marking something as a deprecated public/internal API symbol.
``PREAMBLE <content>``
    Inserted verbatim at the very beginning of the generated header, i.e. *before* the include guard. Intended for adding a start-of-file comment.
``APPENDIX <content>``
    Inserted verbatim at the end of the generated header *before* closing the include guard. Any custom code you want to have in the header should go here.

Output variable:

``<lib_target>_API_HEADER_FILE``
    Set to the full absolute path of the generated header file.
]]
function(prac_configure_library_api lib_target)
    get_property(target_type TARGET ${lib_target} PROPERTY TYPE)
    if (NOT target_type STREQUAL "SHARED_LIBRARY"
            AND NOT target_type STREQUAL "STATIC_LIBRARY"
            AND NOT target_type STREQUAL "OBJECT_LIBRARY"
            AND NOT target_type STREQUAL "MODULE_LIBRARY")
        message(FATAL_ERROR "API can only be configured for libraries. [target: ${lib_target}]")
    endif()

    # ---------- parse and evaluate arguments ----------
    set(switches
        PRAGMA_ONCE OMIT_DEPRECATION_DEFINES
    )
    set(one_value_args
        HEADER_FILE PREFIX INCLUDE_GUARD_NAME STATIC_LIB_DEFINE BUILDING_LIB_DEFINE API_DEFINE
        INTERNAL_DEFINE DEPRECATED_DEFINE DEPRECATED_MSG_DEFINE DEPRECATED_API_DEFINE
        DEPRECATED_API_MSG_DEFINE DEPRECATED_INTERNAL_DEFINE DEPRECATED_INTERNAL_MSG_DEFINE
        PREAMBLE APPENDIX
    )
    cmake_parse_arguments(PARSE_ARGV 1 "" "${switches}" "${one_value_args}" "")

    if (_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown arguments given: ${_UNPARSED_ARGUMENTS}")
    endif()

    if (NOT _PREFIX)
        set(_PREFIX "${lib_target}")
    endif()
    string(MAKE_C_IDENTIFIER "${_PREFIX}" _PREFIX)
    string(TOUPPER "${_PREFIX}" prefix_upper)

    if (_HEADER_FILE)
        get_filename_component(
            _HEADER_FILE "${_HEADER_FILE}" REALPATH BASE_DIR "${CMAKE_CURRENT_BINARY_DIR}")
    else()
        set(_HEADER_FILE "${CMAKE_CURRENT_BINARY_DIR}/${_PREFIX}_api.h")
    endif()
    set(${lib_target}_API_HEADER_FILE "${_HEADER_FILE}" PARENT_SCOPE)

    pracprivate_set_define_names()

    # ---------- configure target ----------
    set_target_properties(${lib_target} PROPERTIES
        CXX_VISIBILITY_PRESET hidden
        C_VISIBILITY_PRESET hidden
        VISIBILITY_INLINES_HIDDEN ON
        DEFINE_SYMBOL ${_BUILDING_LIB_DEFINE}
    )

    if (target_type STREQUAL "STATIC_LIBRARY")
        target_compile_definitions(${lib_target} PUBLIC ${_STATIC_LIB_DEFINE})
    endif()

    # ---------- generate header ----------
    if (NOT _INCLUDE_GUARD_NAME)
        set(_INCLUDE_GUARD_NAME ${prefix_upper}_API_DEFINES)
    endif()
    pracprivate_make_guard_snippet(${_PRAGMA_ONCE} "${_INCLUDE_GUARD_NAME}")
    pracprivate_set_attr_names()
    pracprivate_make_deprecation_defines_section()

    if (_PREAMBLE)
        string(APPEND _PREAMBLE "\n")
    endif()

    if (_APPENDIX)
        string(APPEND _APPENDIX "\n")
    endif()

    configure_file("${pracdetailLibraryApi_here_dir}/LibraryApi.header.in" "${_HEADER_FILE}" @ONLY)
endfunction()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details


# sets: all *_DEFINE variables
macro(pracprivate_set_define_names)
    if (NOT _STATIC_LIB_DEFINE)
        set(_STATIC_LIB_DEFINE ${prefix_upper}_STATIC_LIB)
    endif()
    if (NOT _API_DEFINE)
        set(_API_DEFINE ${prefix_upper}_API)
    endif()
    if (NOT _INTERNAL_DEFINE)
        set(_INTERNAL_DEFINE ${prefix_upper}_INTERNAL)
    endif()
    if (NOT _BUILDING_LIB_DEFINE)
        set(_BUILDING_LIB_DEFINE ${prefix_upper}_BUILDING_LIB)
    endif()
    if (NOT _DEPRECATED_DEFINE)
        set(_DEPRECATED_DEFINE ${prefix_upper}_DEPRECATED)
    endif()
    if (NOT _DEPRECATED_MSG_DEFINE)
        set(_DEPRECATED_MSG_DEFINE ${prefix_upper}_DEPRECATED_MSG)
    endif()
    if (NOT _DEPRECATED_API_DEFINE)
        set(_DEPRECATED_API_DEFINE ${prefix_upper}_DEPRECATED_API)
    endif()
    if (NOT _DEPRECATED_API_MSG_DEFINE)
        set(_DEPRECATED_API_MSG_DEFINE ${prefix_upper}_DEPRECATED_API_MSG)
    endif()
    if (NOT _DEPRECATED_INTERNAL_DEFINE)
        set(_DEPRECATED_INTERNAL_DEFINE ${prefix_upper}_DEPRECATED_INTERNAL)
    endif()
    if (NOT _DEPRECATED_INTERNAL_MSG_DEFINE)
        set(_DEPRECATED_INTERNAL_MSG_DEFINE ${prefix_upper}_DEPRECATED_INTERNAL_MSG)
    endif()
endmacro()


#sets: all *_attr variables
macro(pracprivate_set_attr_names)
    if (MSVC)
        set(export_attr         "__declspec(dllexport)")
        set(import_attr         "__declspec(dllimport)")
        set(hidden_attr         "")
        set(deprecated_attr     "__declspec(deprecated)")
        set(deprecated_msg_attr "__declspec(deprecated(msg))")
    else()
        set(export_attr         "__attribute__((visibility(\"default\")))")
        set(import_attr         "__attribute__((visibility(\"default\")))")
        set(hidden_attr         "__attribute__((visibility(\"hidden\")))")
        set(deprecated_attr     "__attribute__((deprecated))")
        set(deprecated_msg_attr "__attribute__((deprecated(msg)))")
    endif()
endmacro()


#sets: _deprecated_defines_section
macro(pracprivate_make_deprecation_defines_section)
    if (NOT _OMIT_DEPRECATION_DEFINES)
        string(CONFIGURE "
#define @_DEPRECATED_DEFINE@ @deprecated_attr@
#define @_DEPRECATED_MSG_DEFINE@ @deprecated_msg_attr@

#define @_DEPRECATED_API_DEFINE@ @_API_DEFINE@ @_DEPRECATED_DEFINE@
#define @_DEPRECATED_API_MSG_DEFINE@(msg) @_API_DEFINE@ @_DEPRECATED_MSG_DEFINE@(msg)

#define @_DEPRECATED_INTERNAL_DEFINE@ @_INTERNAL_DEFINE@ @_DEPRECATED_DEFINE@
#define @_DEPRECATED_INTERNAL_MSG_DEFINE@(msg) @_INTERNAL_DEFINE@ @_DEPRECATED_MSG_DEFINE@(msg)
"
            deprecation_defines_section @ONLY
        )
    endif()
endmacro()
