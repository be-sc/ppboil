# Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
#[===[.rst:
Convenience module that includes all other Prac modules.
]===]
include_guard(GLOBAL)

include("${CMAKE_CURRENT_LIST_DIR}/Configuration.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/Installation.cmake")  # pulls in Versioning
include("${CMAKE_CURRENT_LIST_DIR}/LibraryApi.cmake")
