# Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
#[===[.rst:
Common internal helper functions. Not part of Prac’s public API.
]===]
include_guard(GLOBAL)

# sets: guard_start, guard_end
function(pracprivate_make_guard_snippet is_pragma_once include_guard_name)
    if (is_pragma_once)
        set(guard_start "#pragma once" PARENT_SCOPE)
        set(guard_end "" PARENT_SCOPE)
    else()
        set(guard_start
            "#ifndef ${include_guard_name}\n#define ${include_guard_name}"
            PARENT_SCOPE
        )
        set(guard_end
            "\n#endif  // ${include_guard_name}"
            PARENT_SCOPE
        )
    endif()
endfunction()
